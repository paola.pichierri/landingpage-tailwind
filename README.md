# Tailwind CSS Lading Page

Template per creare landing page - www.aulab.it

## Come partire

per far partire il motore di Tail Wind esegui questi comandi

```

$ npm i

$ npx tailwindcss -i ./src/input.css -o ./dist/output.css --watch

```

## Setup VSCode

Coders Editor:
- https://code.visualstudio.com/

WebServer per una rapida preview dell'html con auto reload, installa estensione:
- https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer 

Per TailwindCSS estensioni utili di VSCode:
- https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss
- https://marketplace.visualstudio.com/items?itemName=csstools.postcss

## Flusso di Lavoro

1. Lancia il Live Server e visualizza src/index.html

2. Edita il file src/index.html 

3. modella la tua Lading Page