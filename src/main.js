// let counter = 60

// let intervalCounter = setInterval(() => {
// 		counter--
//     document.getElementById('counterElement').style.setProperty('--value', counter)
//     if(counter === 0) {
//         clearInterval(intervalCounter)
//     }
// }, 1000)

let countDownDate = new Date("Jan 5, 2023 15:37:25").getTime();

let x = setInterval(function() {

    // Get today's date and time
    let now = new Date().getTime();
  
    // Find the distance between now and the count down date
    let distance = countDownDate - now;
  
    // Time calculations for days, hours, minutes and seconds
    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
  
    // Display the result in the element with id="demo"
    document.getElementById("counterElement").innerHTML = 
    `
    <div class="grid grid-flow-col gap-5 text-center auto-cols-max">
    <div class="flex flex-col">
    <span class="countdown font-mono text-5xl">
    <span style="--value:15;"></span>
    </span>
    days    
  </div> 
  <div class="flex flex-col">
    <span class="countdown font-mono text-5xl">
      <span style="--value:${hours}"></span>
    </span>
    hours
  </div> 
  <div class="flex flex-col">
    <span class="countdown font-mono text-5xl">
      <span style="--value:${minutes}"></span>
    </span>
    min
  </div> 
  <div class="flex flex-col">
    <span class="countdown font-mono text-5xl">
      <span style="--value:${seconds};"></span>
    </span>
    sec
  </div>
</div>
    `

    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("counterElement").innerHTML = "EXPIRED";
    }
  }, 1000);